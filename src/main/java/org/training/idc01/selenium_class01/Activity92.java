package org.training.idc01.selenium_class01;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Activity92 {

	public static void main(String[] args) {
		WebDriver driver= new FirefoxDriver();

		driver.get("https://www.training-support.net/selenium/drag-drop");
		
		Actions act= new Actions(driver);
		
		act.dragAndDrop(driver.findElement(By.id("draggable")), driver.findElement(By.id("droppable"))).build().perform();
		
		String verify=driver.findElement(By.xpath("//p[@text()='Dropped!']")).getText();
		
		if(verify=="Dropped!")
		{
			System.out.println("dragged and dropped sucessfully");
		}
		else
			System.out.println("Not completed");
		
		
	//	String title= driver.getTitle();
	//	System.out.println(title);
		//driver.findElement(By.id('draggable'))
	//	Actions.drag

	}

}
