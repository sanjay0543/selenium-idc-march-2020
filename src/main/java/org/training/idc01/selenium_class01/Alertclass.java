package org.training.idc01.selenium_class01;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Alertclass {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		WebDriver driver= new FirefoxDriver();

		driver.get("https://www.training-support.net/selenium/javascript-alerts");
		driver.findElement(By.id("simple")).click();
		Alert samplealert= driver.switchTo().alert();  // sample alert is a variable and we can call methods using variable
		String alerttext= samplealert.getText();  
		System.out.println(alerttext);
		samplealert.accept();
		driver.close();
		
	}

}
