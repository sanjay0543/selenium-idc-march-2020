package org.training.idc01.selenium_class01;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Activity2 {

	public static void main(String[] args) {
		WebDriver driver= new FirefoxDriver();

		driver.get("https://www.training-support.net");
		String title= driver.getTitle();
		System.out.println(title);
		driver.findElement(By.tagName("a")).click();
		driver.close();

	}

}
