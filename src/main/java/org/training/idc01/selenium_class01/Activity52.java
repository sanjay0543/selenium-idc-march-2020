package org.training.idc01.selenium_class01;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Activity52 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		WebDriver driver= new FirefoxDriver();

		driver.get("https://www.training-support.net/selenium/dynamic-controls");
		System.out.println(driver.getTitle());
		Boolean enabled=driver.findElement(By.id("input-text")).isEnabled();
		System.out.println(enabled);
		driver.findElement(By.id("toggleInput")).click();
		Boolean disabled=driver.findElement(By.id("input-text")).isEnabled();
		System.out.println(disabled);
		

	}

}
