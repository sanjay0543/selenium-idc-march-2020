package org.training.idc01.selenium_class01;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;


		public class Activity91 {

			public static void main(String[] args) {
				WebDriver driver= new FirefoxDriver();

				driver.get("https://www.training-support.net/selenium/dynamic-attributes");
				WebDriverWait wait= new WebDriverWait(driver,10);
				String title= driver.getTitle();
				System.out.println(title);
				driver.findElement(By.xpath("//input[@placeholder='Username']")).sendKeys("admin");
				driver.findElement(By.xpath("//input[@placeholder='Password']")).sendKeys("password");
				driver.findElement(By.xpath("//button[@type='submit']")).click();
				String logintext=driver.findElement(By.id("action-confirmation")).getText();
				System.out.println(logintext);
				driver.close();

	}

}
