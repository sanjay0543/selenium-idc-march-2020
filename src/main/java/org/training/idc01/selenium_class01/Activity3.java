package org.training.idc01.selenium_class01;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Activity3 {

	public static void main(String[] args) {
		WebDriver driver= new FirefoxDriver();

		driver.get("https://www.training-support.net/selenium/simple-form");
		String title1=driver.getTitle();
		System.out.println(title1);
		WebElement firstname= driver.findElement(By.id("firstName"));
		firstname.sendKeys("sanjay");
		WebElement lastname= driver.findElement(By.id("lastName"));
		lastname.sendKeys("kumar");
		WebElement email= driver.findElement(By.id("email"));
		email.sendKeys("sanjay@kumar.com");
		WebElement phonenumber= driver.findElement(By.id("number"));
		phonenumber.sendKeys("1234567890");
	driver.close();	
	}

}
