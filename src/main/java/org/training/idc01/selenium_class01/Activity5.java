package org.training.idc01.selenium_class01;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Activity5 {

	public static void main(String[] args) {
		WebDriver driver= new FirefoxDriver();

		driver.get("https://www.training-support.net/selenium/dynamic-controls");
		String title=driver.getTitle();
		System.out.println(title);
		Boolean checkbox=driver.findElement(By.xpath("//label[@class='willDisappear']")).isDisplayed();
		System.out.println("Chekcbox is displayed "+checkbox);
		driver.findElement(By.id("toggleCheckbox")).click();
		Boolean Afterclick=driver.findElement(By.xpath("//label[@class='willDisappear']")).isDisplayed();
		System.out.println("Chekcbox is displayed "+Afterclick);
		
	}

}
