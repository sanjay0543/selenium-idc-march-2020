package org.training.idc01.selenium_class01;

import java.util.ArrayList;
import java.util.Iterator;

public class ArraylistActivity1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		ArrayList<String> mylist=new ArrayList();
		
		mylist.add("Hi");
		mylist.add("welcome");
		mylist.add("to");
		mylist.add("selenium");
		mylist.add("training");
		
		Iterator<String> iterator= mylist.iterator(); 
		while(iterator.hasNext()) {
			System.out.println(iterator.next());
		}
		
		System.out.println("using get method");
		
		System.out.println(mylist.get(2));
		
		if(mylist.contains("selenium"))
		{
			System.out.println("the serached name exist'");
		}
		else
			System.out.println("name doesnot exist");

		System.out.println("Size of the arraylist "+mylist.size());
		
		mylist.remove(0);
		System.out.println(mylist);
	}
	

}
