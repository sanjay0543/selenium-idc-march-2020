package org.training.idc01.selenium_class01;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Activity4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		WebDriver driver= new FirefoxDriver();

		driver.get("https://www.training-support.net/selenium/target-practice");
		String title= driver.getTitle();
		System.out.println(title);
		String text=driver.findElement(By.xpath("//h3[@id='third-header']")).getText();
		System.out.println(text);
		String colour=driver.findElement(By.tagName("h5")).getCssValue("color");
		System.out.println("The colour of header5 is "+ colour);
		String att=driver.findElement(By.xpath("//button[text()='Violet']")).getAttribute("class");
		System.out.println(att);
		String colour2=driver.findElement(By.xpath("//button[text()='Grey']")).getText();
		System.out.println(colour2);
		
			}

}
