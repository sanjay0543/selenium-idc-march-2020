package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BasePage {
       
       public WebDriver driver;
       public WebDriverWait wait;
       
       public BasePage(WebDriver driver) {
              this.driver = driver;
              wait = new WebDriverWait(driver, 10);
       }
       
       // wait Helper
       public void waitForVisibility(By element) {
              wait.until(ExpectedConditions.visibilityOfElementLocated(element));
       }
       
       //Click helper
       public void click(By element) {
              waitForVisibility(element);
              driver.findElement(element).click();
       }

       
}
