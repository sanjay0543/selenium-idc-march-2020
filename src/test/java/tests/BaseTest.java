package tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

public class BaseTest {

       public WebDriver driver=null;
       public WebDriverWait wait;
       
       @BeforeTest
       public void setUp() {
              
            //    System.setProperty("webdriver.gecko.driver", "C:\\Users\\veperam.ORADEV\\Desktop\\geckodriver.exe");
                driver = new FirefoxDriver();
                driver.manage().window().maximize();
                driver.manage().deleteAllCookies();
                
       }
       
       @AfterTest
       public void killBrowser() {
              
              driver.close();
              
       }
       
       
}
